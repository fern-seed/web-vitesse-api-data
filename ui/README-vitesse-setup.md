# Vitesse Project

cd ui
git rm -r index.html public/ src/ vite.config.js yarn.lock .gitignore package.json
git add .
git commit -m "get rid of upstream version of UI"

cd ../

## .gitignore is the only conflict

git mv .gitignore .gitignore.orig
git commit -m "make room for new .gitignore temporarily"

git remote add vitesse git@github.com:antfu/vitesse.git
git remote set-url --push vitesse no_push
git fetch vitesse
git merge --allow-unrelated-histories vitesse/main

git mv src vite.config.ts package.json netlify.toml index.html .gitignore public .github tsconfig.json .vscode windi.config.ts .npmrc components.d.ts locales LICENSE pnpm-lock.yaml ui/
git mv README.md ui/README-vitesse.md

git add .
git commit -m "move vitesse into ui directory"

cd ../
git mv .gitignore.orig .gitignore
git commit -m "put this back where it belongs"

configure the containers, servers, and make sure tests run.

docker-compose exec ui bash

rm -r node_modules/\*
